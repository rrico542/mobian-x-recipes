#!/bin/sh

# Our proxy is apt-cacher-ng, which can't handle SSL connections
unset http_proxy

# Allow non-free components
sed -i '/non-free$/s/# //' /etc/extrepo/config.yaml

# Install the Mobian repo
extrepo enable mobian

# Setup repo priorities so mobian comes first
#Package: gnome-control-center gnome-control-center-data gnome-software* libgtk* libwebkit2gtk* gir1.2-gtk* libjavascriptcoregtk* gtk-update-icon-cache desktop-base libgail*
#Pin: release o=Debian
#Pin-Priority: 900
cat > /etc/apt/preferences.d/00-mobian-priority << EOF
Package: *
Pin: release o=Debian
Pin-Priority: 500

Package: *
Pin: release o=Mobian
Pin-Priority: 700

EOF
